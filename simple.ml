(*Expressions module body *)

(*Aluno 1: Guilherme Rito
  Comment: Less is more.*)

(*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
80 columns
*)

type exp =
  Add   of exp * exp
| Sub   of exp * exp
| Mult  of exp * exp
| Div   of exp * exp
| Const of float
| V
| Poly  of float list
;;

let epsilon = 0.000001;;
let step = 0.1;;

let feq f1 f2 = abs_float (f1 -. f2) <= epsilon;;

let pcount l =
  (
    List.length     (List.filter (fun x -> x <> 0.0) l),
    List.fold_right (fun e r -> if e = 0.0 then 1 + r else 0) (List.rev l) 0
  );;

let rec size e =
  match e with
  | V            -> 1
  | Const v      -> 1
  | Add (e1, e2) -> 1 + (size e1) + (size e2)
  | Sub (e1, e2) -> 1 + (size e1) + (size e2)
  | Mult(e1, e2) -> 1 + (size e1) + (size e2)
  | Div (e1, e2) -> 1 + (size e1) + (size e2)
  | Poly l       -> 1 + (let (nz, rz) = pcount l in nz + rz)
;;

let rec eval v e =
  match e with
  | V             -> v
  | Const c       -> c
  | Add  (e1, e2) -> (eval v e1) +. (eval v e2)
  | Sub  (e1, e2) -> (eval v e1) -. (eval v e2)
  | Mult (e1, e2) -> (eval v e1) *. (eval v e2)
  | Div  (e1, e2) -> (eval v e1) /. (eval v e2)
  | Poly []       -> 0.
  | Poly (x::xs)  -> x +. v *. eval v (Poly xs)
;;

let evalConst = eval 0.0;;

let rec deriv e =
  match e with
  | V             -> Const 1.
  | Const c       -> Const 0.
  | Add  (e1, e2) -> Add (deriv e1, deriv e2)
  | Sub  (e1, e2) -> Sub (deriv e1, deriv e2)
  | Mult (e1, e2) -> let (e1', e2') = (deriv e1, deriv e2) in
                     Add (Mult (e1', e2), Mult(e1, e2'))
  | Div  (e1, e2) -> let (e1', e2') = (deriv e1, deriv e2) in
                     Div (Sub (Mult (e1', e2), Mult (e1, e2')), Mult (e2, e2))
  | Poly []       -> Const 0.
  | Poly l        ->
     let fold_fun = (fun (d,r) e ->
         (d +. 1., if d = 0. then [] else (e *. d)::r)) in
     let (_, l') = List.fold_left fold_fun (0.0, []) l in
     Poly l'
(*| Poly(x::xs) -> Add(Poly xs, Mult(V, deriv (Poly xs)))
    other (even prettier) solution. *)
;;



let rec alike a n e1 e2 =
  (n = 0) || (feq (eval a e1) (eval a e2) && alike (a +. step) (n - 1) e1 e2)
;;

let rec newton s e =
  let es = eval s e in
  if feq es 0.0 then s  else newton (s -. es /. (eval s (deriv e))) e
;;



(******************* simpl (a function in a league of its own) *******************)

(* Eliminate unnecessary stuff *)
let simplList l =
  List.rev
    (List.fold_left
       (fun r e -> if e = 0.0 then e::r else []) 
       (List.rev l)
       []
    )
;;

(* Is the expression constant? *)
let rec isConst e =
  match e with
  | Const c      -> true
  | Poly []      -> true
  | Poly [x]     -> true
  | Add  (e1,e2) -> ((isConst e1) && (isConst e2))
  | Sub  (e1,e2) -> ((isConst e1) && (isConst e2))
  | Mult (e1,e2) -> ((isConst e1) && (isConst e2))
  | Div  (e1,e2) -> ((isConst e1) && (isConst e2)) 
  | _            -> false
;;

let opPoly op = List.map2 (fun x y -> op x y);;
let divPoly v = List.map  (fun x   -> x /. v);;

let rec makeList s v = if s = 0 then [] else (v :: (makeList (s - 1) v));;

(* Make two polynomials compatible, by makin them the same size *)
let rec makeCompatible l1 l2 =
  match (l1, l2) with
  | ([]   , []   ) -> ([]                           ,  [])
  | ([]   , _    ) -> (makeList (List.length l2) 0.0,  l2)
  | (_    , []   ) -> (l1, makeList (List.length l1)  0.0)
  | (x::xs, y::ys) -> let (a, b) = makeCompatible xs ys in
                      (x::a, y::b)
;;

let rec multiplyPolys l1 l2 =
  match l1 with
  | []    -> []
  | [x]   -> List.map     (fun v -> v *. x) l2
  | x::xs -> x::(List.map (fun v -> v *. x) (multiplyPolys xs l2))
;;

  
(* Make a polynomial from the expression, then simplify it if possible *)
let rec polynomialize e = 
  match e with
  | V            ->
     Poly [0.; 1.]
  | Const c      ->
     Poly [c]
  | Add  (e1,e2) ->
     let (p1,p2) = (polynomialize e1, polynomialize e2) in
     sumExpression          (Add  (p1, p2)) p1 p2
  | Sub  (e1,e2) ->
     let (p1,p2) = (polynomialize e1, polynomialize e2) in
     subExpression          (Sub  (p1, p2)) p1 p2
  | Mult (e1,e2) ->
     let (p1,p2) = (polynomialize e1, polynomialize e2) in
     multiplyFactExpression (Mult (p1, p2)) p1 p2
  | Div  (e1,e2) ->
     let (p1,p2) = (polynomialize e1, polynomialize e2) in 
     divideFactExpression   (Div  (p1, p2)) p1 p2
  | Poly l       ->
     e
and sumExpression e e1 e2 =
  let sumQuotient = (fun a num den ->
      Div (polynomialize (Add (Mult (a, den), num)), den)) in
  match (e1, e2) with
  | (a, b) when a = b              ->
     polynomialize (Mult (Const 2., a)) (* Useless optimization *)
  | (Poly l, Poly r)               ->
     let (lp, rp) = makeCompatible l r    in
     Poly (opPoly (+.) lp rp)
  | (a, Div (num, den))            ->
     let opt      = sumQuotient a num den in
     if size opt > size e then e else opt
  | (Div (num, den), a)            ->
     let opt      = sumQuotient a num den in
     if size opt > size e then e else opt 
  | (_, _)                         ->
     e
and subExpression e e1 e2 =
  let subQuotient = (fun a num den   ->
      Div (polynomialize (Sub (Mult (a, den), num)), den)) in
  match (e1, e2) with
  (*Not correct that if e1 = e2 then Sub (e1, e2) is equivalent to Const 0.*)
  | (Poly l, Poly r)                 ->
     let (lp, rp) = makeCompatible l r    in
     Poly (opPoly (-.) lp rp)
  | (Div (e11, e12), Div (e21, e22)) ->
     let opt      = Div (
                        polynomialize (Sub (Mult (e11, e22), Mult (e21, e12))),
                        polynomialize (Mult (e12, e22))
                      ) in
     if size opt > size e then e else opt
  | (a, Div (num, den))              ->
     let opt      = subQuotient a num den in
     if size opt > size e then e else opt
  | (Div (num, den), a)              ->
     let opt      = subQuotient a num den in
     if size opt > size e then e else opt 
  | (_, _)                           ->
     e
and multiplyFactExpression e e1 e2 =
  if (   (isConst e1 && 0. = evalConst e1))
     || ((isConst e2 && 0. = evalConst e2)) then
    polynomialize (Const 0.)
  else
    match (e1, e2) with
    | (Poly l1, Poly l2)               ->
       Poly (multiplyPolys l1 l2)
    | (Poly l1, Div (e21, e22))        ->
       Div (polynomialize (Mult (e1, e21)), e22)
    | (Div (e11, e12), Poly l2)        ->
       Div(polynomialize (Mult (e11, e2)), e12)
    | (Div (e11, e12), Div (e21, e22)) ->
       Div (polynomialize (Mult (e11, e21)), polynomialize (Mult (e12, e22)))
    | (_, _)                           ->
       e
and divideFactExpression e e1 e2 =
  if (isConst e1 && 0. = evalConst e1) then
    polynomialize (Const 0.)
  else
    match (e1, e2) with
    | (Div (e11, e12), Div (e21, e22)) ->
       let (p1n, p2n) = (
           polynomialize (Mult (e11, e22)),
           polynomialize (Mult (e12, e21))
         ) in
       Div (p1n, p2n)
    | (Div (e11, e12), Poly l2)        ->
       Div (e11, polynomialize (Mult (e12, e2)))
    | (Poly l1, Div (e21, e22))        ->
       Div (polynomialize (Mult (e1, e22)), e21)
    | (Poly l1, Poly l2)               ->
       if (isConst e1) && (isConst e2) then
         Poly ([evalConst (Div (e1, e2))])
       else if (isConst e2) then
         Poly (divPoly (evalConst e2) l1)
       else if (isConst e1) then (*just to be pretty*) 
         Div (Const 1., Poly (divPoly (evalConst e1) l2))
       else
         e
    | (_, _)                           ->
       e
;;

(* Find constant expressions *)
let rec constify e =
  if (isConst e) then
    Const (evalConst e)
  else 
    match e with
    | V               ->
       V
    | Poly [0.; 1.]   ->
       V
    | Const c         ->
       Const c
    | Add  (e1 , e2 ) ->
       let (e1', e2') =
         (
           (if isConst e1 then Const (evalConst e1) else e1),
           (if isConst e2 then Const (evalConst e2) else e2)
         ) in
       Add (e1', e2')
    | Sub  (e1 , e2 ) ->
       let (e1', e2') = 
         (
           (if isConst e1 then Const (evalConst e1) else e1),
           (if isConst e2 then Const (evalConst e2) else e2)
         ) in
       Sub (e1', e2') 
    | Mult (e1 , e2 ) ->
       let (e1', e2') = 
         (
           (if isConst e1 then Const (evalConst e1) else e1),
           (if isConst e2 then Const (evalConst e2) else e2)
         ) in
       Mult(e1', e2') 
    | Div  (e1 , e2 ) ->
       let (e1', e2') = 
         (
           (if isConst e1 then Const (evalConst e1) else e1),
           (if isConst e2 then Const (evalConst e2) else e2)
         ) in
       Div (e1', e2') 
    | Poly l          ->
       Poly (simplList l)
;;

let simpl e = constify (polynomialize e);;

(***************** graph *******************)

let near valueY markingY stepY =
  let bottomY = markingY -. stepY /. 2.0 in
  let topY    = markingY +. stepY /. 2.0 in
  bottomY < valueY && valueY <= topY
;;

let rec bounds_y nx sx e =
  let v = eval sx e in
  match nx with
  | 0 -> failwith "Expressions.bounds_y nx = 0"
  | 1 -> (v, v)
  | _ -> let (l, r) = bounds_y (nx - 1) (sx +. step) e in
         (min v l, max v r)
;;

let rec graph_line bx sx nx by sy ny e =
  if nx = 0 then
    ""
  else
    let ey = eval bx e in
    (
      if      near ey  by sy then
        "*"
      else if near 0.0 by sy then
        "-"
      else if near 0.0 bx sx then
        "|"
      else
        " "
    ) ^ (graph_line (bx +. sx) sx (nx - 1) by sy ny e)
;;

let rec build_graph bx sx nx by sy ny e =
  if ny = 0 then [] else
    (graph_line bx sx nx by sy ny e)
    ::(build_graph bx sx nx (by +. sy) sy (ny - 1) e)
;;


let graph nx ny s e = 
  let (min_y, max_y) = bounds_y nx s e                             in
  let step_y         = (max_y -. min_y) /. (float_of_int (ny - 1)) in
  List.rev (build_graph s step nx min_y step_y ny e)
;;
